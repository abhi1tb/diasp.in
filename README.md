# Diasp.in Landing Page

This is the landing page that can be seen at [diasp.in](https://diasp.in).

Although it was supposed to be modified [the way it is said in the official documentation](https://wiki.diasporafoundation.org/Custom_splash_page), we are overriding it, the way they ask not to in the official documentation. This is because when loading custom splash page, diaspora adds its own css, links, etc which are incompatible with the theme of diasp.in homepage.

So, index.html is pushed to public/ folder thereby making the server serve the homepage directly.

The assets are pushed to public/assets/homepage/ folder.

push.sh has the commands necessary to scp the files to their right locations. You need to have diasp.in configured in .ssh/config to make it work.
