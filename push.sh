# Run these commands in /usr/share/diaspora/public
# sudo setfacl -m u:debian:rwx /usr/share/diaspora/public
# sudo setfacl -m u:debian:rwx /usr/share/diaspora/public/assets
# sudo setfacl -m u:debian:rwx /usr/share/diaspora/public/assets/homepage 
# sudo setfacl -m u:debian:rwx /usr/share/diaspora/public/assets/homepage/images

copyassets () {
  echo "Pushing assets"
  rsync -rv --progress css fonts images debian@diasp.in:/usr/share/diaspora/public/assets/homepage
}
copyhome () {
  echo "Pushing index.html"
  rsync -v --progress index.html debian@diasp.in:/usr/share/diaspora/public/index.html
}

gitpush () {
  git commit -am "$1"
  git push
}

if [ "$1" == "assets" ]; then
  copyassets
elif [ "$1" == "home" ]; then
  copyhome
else
  echo "Will copy both assets and home to their locations. If you want to do just one, please use ./push.sh assets or ./push.sh home"
  copyassets
  copyhome
fi
if [ -z "$2" ]; then
  if [ -z "$1" ]; then
    gitpush "some changes"
  else
    gitpush "$1"
  fi
else
  gitpush "$2"
fi
